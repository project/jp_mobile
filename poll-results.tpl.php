<?php

/**
 * @file
 * Inserts inline CSS.
 */
?>
<div class="poll">
  <?php print $results; ?>
  <div class="total" style="text-algin:center";>
    <?php print t('Total votes: @votes', array('@votes' => $votes)); ?>
  </div>
  <?php if (!empty($cancel_form)): ?>
    <?php print $cancel_form; ?>
  <?php endif; ?>
</div>
