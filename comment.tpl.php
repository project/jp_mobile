<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; ?> clear-block" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;">
  <?php print $picture ?>
  <?php if ($comment->new) : ?>
  <a id="new"></a>
  <span class="new"><?php print $new ?></span>
  <?php endif; ?>

  <h3 style="font-size:small"><?php print $title ?></h3>
  <div class="submitted" style="font-size:x-small;">
    <?php print $submitted ?>
  </div>
  <div class="content">
    <?php print $content ?>
    <?php if ($signature): ?>
    <div class="clear-block">
      <div>---</div>
      <?php print $signature ?>
    </div>
    <?php endif; ?>
  </div>
  <div class="links" style="font-size:x-small;">
    <?php print $links ?>
  </div>
</div>
