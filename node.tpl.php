<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($picture) {
    print $picture;
  }?>
  <?php if ($page == 0) { ?><h2 class="title" style="font-size:small;margin:0px;"><a href="<?php print $node_url ?>"><?php print $title ?></a></h2><?php }; ?>
  <span class="submitted" style="font-size:x-small;"><?php print $submitted ?></span>
  <span class="taxonomy" style="font-size:x-small;"><?php print $terms ?></span>
  <div class="content"><?php print $content ?></div>
  <?php if ($links) { ?><div class="links" style="font-size:x-small;"><?php print $links ?></div><?php }; ?>
  <?php if ($page == 0) { ?><hr style="margin:0px;" /><?php }; ?>
</div>
