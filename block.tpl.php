<div class="block block-<?php print $block->module; ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;background-color:#eeeeff;">
  <?php if ($block->subject): ?>
  <div class="title" style="background-color:#6699cc;color:#ffffff;"><?php print $block->subject; ?></div>
  <?php endif; ?>
  <div class="content"><?php print $block->content; ?></div>
</div>
