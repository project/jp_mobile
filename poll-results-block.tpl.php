<?php
/**
 * @file
 * Inserts inline CSS.
 */
?>

<div class="poll">
  <div class="title"><?php print $title ?></div>
  <?php print $results ?>
  <div class="total" style="text-align:center;">
    <?php print t('Total votes: @votes', array('@votes' => $votes)); ?>
  </div>
</div>
<div class="links"><?php print $links; ?></div>
