<?php
$user_agent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/^DoCoMo/', $user_agent)) {
  $inc = 'docomo_header.inc';
}
else {
  $inc = 'au_softbank_header.inc';
}
require_once(path_to_theme() .'/'. $inc);
print $mobile_head;
?>
  <body> 
    <div id="wrapper" style="width:100%;font-family:arial,sans-serif;font-size:small;">
      <div id="header" style="background-color:#ffffcc;border-bottom-width:1px;border-bottom-color:#000000;border-bottom-style:solid;">
        <?php if ($site_name): ?>
          <div class="site_name"><span><?php print $site_name; ?></span></div>
        <?php endif; ?>
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" style="text-decoration:none;"><img src="<?php print $logo; ?>" alt="<?php if ($site_name): print $site_name;  endif; ?>" class="logo" style="border-style:none;" /></a>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <div class="slogan"><?php print variable_get('site_slogan', ''); ?></div>
        <?php endif; ?>
        <div class="navigation" style="background-color:#ffffcc;">
          <?php print theme('links', $primary_links); ?>
          <?php if ($secondary_links): ?>
          <div class="secondary_links"> <?php print theme('links', $secondary_links); ?></div>
          <?php endif; ?>
          <?php if ($search_box): ?>
            <span class="search_box"><?php print $search_box; ?></span>
          <?php endif; ?>
        </div>
      </div>
      <div class="container">
        <?php if ($breadcrumb): ?>
        <div class="breadcrumb" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;background-color:#ffeeee;"><?php print $breadcrumb; ?></div>
        <?php endif; ?>
        <?php if ($content_top): ?>
        <div id="content_top"><?php print $content_top; ?></div>
        <?php endif; ?>
        <div id="page" style="background-color:#eeffee;">
          <div id="content">
            <?php if ($messages): ?>
            <div class="message_box" style="background-color:#ffffaa;"><?php print $messages; ?></div>
            <?php endif; ?>
            <?php if ($is_front && $mission): ?>
            <div class="mission" style="background-color:#ccffcc;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;"><?php print $mission; ?></div>
            <?php endif; ?>
            <?php if ($title): ?>
            <div style="background-color:#66cc33;color:#ffffff;">
              <h1 class="title" style="font-size:medium;margin:0px;"><?php print $title; ?></h1>
            </div>
            <?php endif; ?>
            <div class="tabs" style="font-size:x-small">
              <?php print $tabs; ?>
            </div>
            <?php print $content; ?>
          </div>
        </div>
        <?php if ($content_bottom): ?>
        <div id="content_bottom"><?php print $content_bottom; ?></div>
        <?php endif; ?>
      </div>
      <div id="footer" style="background-color:#dddddd;"><?php print $footer_message; ?></div>
    </div>
  </body>
</html>
