<?php

/**
 * @file
 * Theme functions for the jp_mobile theme.
 */


/**
 * Converts HTML entities to decimal forms for xml parsing.
 * Issues:
 * 1. It appears some html entities are missing from the translation table.
 * 2. Softbank emulator doesn't parse some characters in either html entity or numeric point form.
 * Hold back from using this function until the above issues are solved.
 *
 * @param $string
 *  A string containing html entities.
 * @return 
 *  A string containing html numeric code points.
 */
/*
function numeric_entities($string) {
  $mapping = array();
  foreach (get_html_translation_table(HTML_ENTITIES) as $char => $entity) {
    $mapping[$entity] = '&#' . ord($char) . ';';
  }
  return str_replace(array_keys($mapping), $mapping, $string);
}
 */

/**
 * Converts encoding from utf-8 to Shift JIS.
 */
function phptemplate_render_template($template_file, $variables) {
  extract($variables, EXTR_SKIP);  // Extract the variables to a local namespace
  ob_start();                      // Start output buffering
  include "./$template_file";      // Include the template file
  $contents = ob_get_contents();   // Get the contents of the buffer
  ob_end_clean();                  // End buffering and discard
  if (strpos($template_file, 'page.tpl.php')) {
    $contents = mb_convert_encoding($contents, 'sjis', 'utf-8');
    //Softbank can't interpret numeric codes.
    $contents = str_replace('&#039;', '\'', $contents);
  }
  return $contents;                // Return the contents
} 

/**
 * Inserts Shift_JIS into accept-charset.
 */
function jp_mobile_form($element) {
  // Anonymous div to satisfy XHTML compliance.
  $action = $element['#action'] ? 'action="'. check_url($element['#action']) .'" ' : '';
  return '<form '. $action .' accept-charset="UTF-8;q=1, Shift_JIS;q=0.5" method="'. $element['#method'] .'" id="'. $element['#id'] .'"'. drupal_attributes($element['#attributes']) .">\n<div>". $element['#children'] ."\n</div></form>\n";
}

/**
 * Replaces raquo with '>>'.
 */
function jp_mobile_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode('&gt;&gt;', $breadcrumb) .'</div>';
  }
}

/**
 * Replaces <ul> with <div>
 */
function jp_mobile_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<div class=\"tabs primary\">\n". $primary ."</div>\n";
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= "<div class=\"tabs secondary\">\n". $secondary ."</div>\n";
  }

  return $output;
}

/**
 * Replaces <li> with <span> and adds a delimiter.
 */
function jp_mobile_menu_local_task($link, $active = FALSE) {
  return '<span '. ($active ? 'class="active" ' : '') .'>'. $link ."</span> |\n";
}

/**
 * Replaces <ul> and <li> with <span>, and adds a link delimiter.
 */
function jp_mobile_links($links, $attributes = array('class' => 'links')) {
  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<span'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      //add delimiter between links.
      if ($i != 1) {
        $output .= '| ';
      }
      $output .= '<span'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</span>\n";
    }

    $output .= '</span>';
  }

  return $output;
}

/**
 * Replaces PNG with GIF and removes border.
 */
function jp_mobile_feed_icon($url, $title) {
  if ($image = theme('image', 'sites/all/themes/jp_mobile/feed.gif', t('Syndicate content'), $title, array('style' => 'border-style:none'))) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
  }
}

/**
 * Replaces PNG with GIF and removes border.
 */
function jp_mobile_xml_icon($url) {
  if ($image = theme('image', 'sites/all/themes/jp_mobile/xml.gif', t('XML feed'), t('XML feed'), array('style' => 'border-style:none'))) {
    return '<a href="'. check_url($url) .'" class="xml-icon">'. $image .'</a>';
  }
}

/**
 * Strips $base_url off links so that session id will be appended to them.
 */
function jp_mobile_preprocess_search_result(&$variables) {
  global $base_url;
  $variables['url'] = url(str_replace($base_url .'/', '', $variables['url']));
}


/**
 * Replaces angle quotes with '<' and '>'.
 */
function jp_mobile_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = str_replace('amp;', '', theme('pager_first', (isset($tags[0]) ? $tags[0] : t('&lt;&lt; first')), $limit, $element, $parameters));
  $li_previous = str_replace('amp;', '', theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('&lt; previous')), $limit, $element, 1, $parameters));
  $li_next = str_replace('amp;', '', theme('pager_next', (isset($tags[3]) ? $tags[3] : t('next &gt;')), $limit, $element, 1, $parameters));;
  $li_last = str_replace('amp;', '', theme('pager_last', (isset($tags[4]) ? $tags[4] : t('last &gt;&gt;')), $limit, $element, $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => 'pager-first',
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => 'pager-previous',
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => 'pager-current',
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => 'pager-next',
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => 'pager-last',
        'data' => $li_last,
      );
    }
    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
  }
}

